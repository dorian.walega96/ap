package com.example.ap.utils;

import androidx.recyclerview.widget.RecyclerView;

import com.example.ap.activities.MainActivity;
import com.example.ap.databinding.PostViewBinding;
import com.example.ap.model.PostViewModel;

public class PostViewHolder extends RecyclerView.ViewHolder{

    private PostViewBinding binding;

    public PostViewHolder(PostViewBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(PostViewModel item) {
        binding.setPost(item);
        binding.like.setOnClickListener(view -> reaction());
        binding.content.setOnClickListener(view -> showPost());
        binding.executePendingBindings();
    }

    private void reaction() {
        ((MainActivity) this.itemView.getContext()).reaction(binding.getPost());
    }

    private void showPost() {
        ((MainActivity) this.itemView.getContext()).showPost(binding.getPost());
    }
}
