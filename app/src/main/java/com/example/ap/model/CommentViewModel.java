package com.example.ap.model;

import java.util.ArrayList;

public class CommentViewModel {

    private Comment comment;
    private ArrayList<CommentReaction> reactions;

    public CommentViewModel(Comment post, ArrayList<CommentReaction> reactions) {
        this.comment = post;
        this.reactions = reactions;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public ArrayList<CommentReaction> getReactions() {
        return reactions;
    }

    public void setReactions(ArrayList<CommentReaction> reactions) {
        this.reactions = reactions;
    }
}
