package com.example.ap.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Reaction extends RealmObject {

    @PrimaryKey
    private int id;
    private int postId;
    private int authorId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
