package com.example.ap.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CommentReaction extends RealmObject {

    @PrimaryKey
    private int id;
    private int commentId;
    private int authorId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
