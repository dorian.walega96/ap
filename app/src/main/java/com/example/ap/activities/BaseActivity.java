package com.example.ap.activities;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ap.model.User;

public class BaseActivity extends AppCompatActivity {

    protected static User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        BaseActivity.user = user;
    }
}
