package com.example.ap.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.ap.R;
import com.example.ap.databinding.ActivityLoginBinding;
import com.example.ap.model.User;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding binding;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setBindings();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
    }

    public void setBindings() {
        binding.btnRegister.setOnClickListener(view -> startActivity(new Intent(this, RegisterActivity.class)));
        binding.btnLogin.setOnClickListener(view -> {
            if (loginUser(binding.getLogin(), binding.getPassword())) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("login", binding.getLogin());
                startActivity(intent);
            } else {
                Toast.makeText(this, "Wrong login or password!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean loginUser(String login, String password) {
        User user = realm.where(User.class).equalTo("login", login).findFirst();
        if (user == null) {
            return false;
        }
        return user.getPassword().equals(password);
    }
}
