package com.example.ap.activities;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.ap.R;
import com.example.ap.databinding.ActivityMainBinding;
import com.example.ap.model.Post;
import com.example.ap.model.PostViewModel;
import com.example.ap.model.Reaction;
import com.example.ap.model.User;
import com.example.ap.utils.PostRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends BaseActivity {

    private static int ADD_POST = 0;

    ActivityMainBinding binding;
    private PostRecyclerViewAdapter postRecyclerViewAdapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        String login = getIntent().getExtras().getString("login");
        user = realm.where(User.class).equalTo("login", login).findFirst();
        setupRecyclerView();
        binding.btnAdd.setOnClickListener(view -> startActivityForResult(new Intent(this, AddActivity.class), ADD_POST));
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = binding.postRecyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        postRecyclerViewAdapter = new PostRecyclerViewAdapter(downloadPosts());
        recyclerView.setAdapter(postRecyclerViewAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ADD_POST) {
            refreshPosts();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void refreshPosts() {
        postRecyclerViewAdapter.setPostList(downloadPosts());
        postRecyclerViewAdapter.notifyDataSetChanged();
    }

    private ArrayList<PostViewModel> downloadPosts() {
        RealmResults<Post> realmPosts = realm.where(Post.class).sort("date", Sort.DESCENDING).findAll();
        ArrayList<Post> posts = new ArrayList<>(realm.copyFromRealm(realmPosts));
        ArrayList<PostViewModel> postModels = new ArrayList<>();
        posts.forEach(post -> {
            RealmResults<Reaction> reactions = realm.where(Reaction.class).equalTo("postId", post.getId()).findAll();
            ArrayList<Reaction> reactionModels = new ArrayList<>();
            reactionModels.addAll(realm.copyFromRealm(reactions));
            postModels.add(new PostViewModel(post, reactionModels));
        });
        return postModels;
    }

    public void reaction(PostViewModel model) {
        if (model.getReactions().stream().filter(reaction -> reaction.getAuthorId() == getUser().getId()).toArray().length == 0) {
            Number id = realm.where(Reaction.class).max("id");
            int newId = 0;
            if (id != null) newId = id.intValue() + 1;

            Reaction reaction = new Reaction();
            reaction.setId(newId);
            reaction.setAuthorId(user.getId());
            reaction.setPostId(model.getPost().getId());
            realm.beginTransaction();
            realm.copyToRealm(reaction);
            realm.commitTransaction();
            refreshPosts();
        } else {
            realm.beginTransaction();
            realm.where(Reaction.class)
                    .equalTo("postId", model.getPost().getId())
                    .equalTo("authorId", user.getId())
                    .findAll().deleteAllFromRealm();
            realm.commitTransaction();
            refreshPosts();
        }
    }

    public void showPost(PostViewModel model) {
        Intent intent = new Intent(this, PostActivity.class);
        Bundle bundle = new Bundle(1);
        bundle.putInt("postId", model.getPost().getId());
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
