package com.example.ap.activities;

import android.os.Bundle;

import com.example.ap.R;
import com.example.ap.databinding.ActivityPostBinding;

import androidx.databinding.DataBindingUtil;

public class PostActivity extends BaseActivity {

    ActivityPostBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }
}
