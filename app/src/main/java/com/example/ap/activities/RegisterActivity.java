package com.example.ap.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.ap.R;
import com.example.ap.databinding.ActivityRegisterBinding;
import com.example.ap.model.Post;
import com.example.ap.model.User;

import io.realm.Realm;
import io.realm.RealmResults;

public class RegisterActivity extends AppCompatActivity {

    ActivityRegisterBinding binding;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        realm = Realm.getDefaultInstance();
        setBindings();
    }

    private void setBindings() {
        binding.setNewUser(new User());
        binding.setPassCheck("");
        binding.btnRegister.setOnClickListener(view -> {
            boolean result = registerNewUser();
            if (result) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("login", binding.getNewUser().getLogin());
                startActivity(intent);
            }
        });
    }

    private boolean registerNewUser() {
        User newUser = binding.getNewUser();
        if (validateNewUser(newUser)) {
            Number id = realm.where(User.class).max("id");
            int newId = 0;
            if (id != null) newId = id.intValue() + 1;
            newUser.setId(newId);

            realm.beginTransaction();
            User registeredUser = realm.copyToRealm(newUser);
            realm.commitTransaction();
            return registeredUser != null;
        } else {
            return false;
        }
    }

    private boolean validateNewUser(User user) {
        RealmResults<User> users = realm.where(User.class).equalTo("login", user.getLogin()).findAll();
        if (!users.isEmpty()) {
            Toast.makeText(this, "User already exists!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!user.getPassword().equals(binding.getPassCheck())) {
            Toast.makeText(this, "Password do not match!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (user.getPassword().length() < 4) {
            Toast.makeText(this, "Password too short!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
